package com.webinfotech.memestan.threading;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class Templates_Thread extends Thread
{
    public interface Callback {
        void onFileSavingFinish(String uri_templates);
    }

    Context mContext;
    String templateUri;
    Callback mCallback;

    public Templates_Thread(Context mContext, String templateUri, Callback mCallback) {
        this.mContext = mContext;
        this.templateUri = templateUri;
        this.mCallback = mCallback;
    }

    @Override
    public void run() {
        super.run();
        try {
            int count;
            URL url = new URL(templateUri);
           URLConnection urlConnection = url.openConnection();
           urlConnection.setRequestProperty("Accept-Encoding", "identity");
           urlConnection.connect();
           int length = urlConnection.getContentLength();
           InputStream inputStream = new BufferedInputStream(url.openStream());
           String tempName=String.format("%d.jpg",System.currentTimeMillis());
           File outputFile = new File(Environment.getExternalStorageDirectory(),"/MEME/"+ tempName);
           OutputStream outputStream = new FileOutputStream(outputFile);
           byte data[] = new byte[length];
           while ((count = inputStream.read(data)) != -1) {
               outputStream.write(data, 0, count);
           }
           outputStream.flush();
           outputStream.close();
           inputStream.close();
           mCallback.onFileSavingFinish(outputFile.getAbsolutePath());
        }catch (Exception e) {
        }
    }

}
