package com.webinfotech.memestan.threading;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;



public class FfmpegThreading extends Thread {

    public interface Callback {
        void onProgressChanged(int progress);
        void onTaskFinish();
    }

    Context mContext;
    String[] command;
    int duration;
    Callback mCallback;
    FFmpeg ffmpeg;

    public FfmpegThreading(Context context, String[] command, int duration, Callback callback) {
        this.mContext = context;
        this.command = command;
        this.duration = duration;
        this.mCallback = callback;
    }

    @Override
    public void run() {
        try {
            LoadFFMpegBinary();
            startFfmpeg();
        } catch (Exception e) {
        }
    }

    public void startFfmpeg()
    {

        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler()
            {

                @Override
                public void onStart() {
                }

                @Override
                public void onProgress(String message) {
                    String arr[];
                    if (message.contains("time="))
                    {
                        arr=message.split("time=");
                        String mainString=arr[1];
                        String timeString[]=mainString.split(":");
                        String [] secondString=timeString[2].split(" ");
                        String seconds=secondString[0];

                        int hours=Integer.parseInt(timeString[0]);
                        hours=hours*3600;
                        int min=Integer.parseInt(timeString[1]);
                        min=min*60;
                        float sec=Float.valueOf(seconds);
                        float timeinsec=hours+min+sec;
                        mCallback.onProgressChanged((int)(timeinsec/duration*100));
                    }
                }

                @Override
                public void onFailure(String message) { }

                @Override
                public void onSuccess(String message) { }

                @Override
                public void onFinish() {
                    mCallback.onTaskFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }


    private void LoadFFMpegBinary() throws Exception
    {

        if (ffmpeg==null)
        {
            ffmpeg= FFmpeg.getInstance(mContext);
        }
        ffmpeg.loadBinary(new LoadBinaryResponseHandler()
        {

            @Override
            public void onFailure() {
                super.onFailure();
            }

            @Override
            public void onSuccess() {
                super.onSuccess();
            }
        });
    }

    public void cancelTask() {
        ffmpeg.killRunningProcesses();
        ffmpeg = null;
    }

}
