package com.webinfotech.memestan.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

public interface AppRepository {

    @GET("template/list")
    Call<ResponseBody> getTemplates();

    @GET("sticker/list")
    Call<ResponseBody> getStickers();

}
