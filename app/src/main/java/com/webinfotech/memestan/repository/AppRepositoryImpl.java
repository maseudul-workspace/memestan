package com.webinfotech.memestan.repository;

import android.util.Log;

import com.google.gson.Gson;
import com.webinfotech.memestan.domain.models.ImagesWrapper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = APIclient.createService(AppRepository.class);
    }

    public ImagesWrapper getTemplates() {
        ImagesWrapper imagesWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> getTemplates = mRepository.getTemplates();

            Response<ResponseBody> response = getTemplates.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    imagesWrapper = null;
                }else{
                    imagesWrapper = gson.fromJson(responseBody, ImagesWrapper.class);

                }
            } else {
                imagesWrapper = null;
            }
        }catch (Exception e){
            imagesWrapper = null;
        }
        return imagesWrapper;
    }

    public ImagesWrapper getStickers() {
        ImagesWrapper imagesWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> getTemplates = mRepository.getStickers();
            Response<ResponseBody> response = getTemplates.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    imagesWrapper = null;
                }else{
                    imagesWrapper = gson.fromJson(responseBody, ImagesWrapper.class);

                }
            } else {
                imagesWrapper = null;
            }
        }catch (Exception e){
            imagesWrapper = null;
        }
        return imagesWrapper;
    }

}
