package com.webinfotech.memestan.presentation.ui.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.memestan.R;
import com.webinfotech.memestan.domain.models.Images;
import com.webinfotech.memestan.domain.models.Template_class;
import com.webinfotech.memestan.presentation.ui.adapters.ImageListAdapter;
import com.webinfotech.memestan.presentation.ui.adapters.TemplatesAdapter;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddPicSearchTemplateDialog implements ImageListAdapter.Callback, TemplatesAdapter.Callback {

    public interface Callback
    {
        void setPicToView(String url);
        void chooseTemplate();
    }

    Callback addPicCallback;
    Context mContext;
    Activity mActivity;
    View addPicdialogContainer;
    AlertDialog.Builder addPicbuilder;
    AlertDialog addPicdialog;
    @BindView(R.id.search_template_recyclerview)
    RecyclerView search_template_recycler;
    @BindView(R.id.edt_txt_search_view)
    AutoCompleteTextView searchTemplateEdit;
    ArrayList<Template_class> searchTemplate;
    ImageListAdapter imageListAdapter;
    TemplatesAdapter templatesAdapter;
    String[] imageNames;
    Images[] images;

    public AddPicSearchTemplateDialog(Callback addPicCallback, Context mContext, Activity mActivity) {
        this.addPicCallback = addPicCallback;
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public  void setUpDialog()
    {
        addPicdialogContainer=mActivity.getLayoutInflater().inflate(R.layout.add_pic_search_dialog,null);
        addPicbuilder=new AlertDialog.Builder(mContext,android.R.style.Theme_DeviceDefault_NoActionBar_Fullscreen);
        addPicbuilder.setView(addPicdialogContainer);
        addPicdialog=addPicbuilder.create();
        ButterKnife.bind(this,addPicdialogContainer);
        setSearchTemplateEditTextListeners();
        setSearchTemplateEditEditorListener();
    }

    private void setAutocompleteListener() {
        searchTemplateEdit.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                searchTemplate = new ArrayList<>();
                for (int i = 0; i < images.length; i++) {
                    if (images[i].name.contains(imageNames[position])) {
                        searchTemplate.add(new Template_class(images[position].id, images[position].name, images[position].image));
                    }
                }
                imageListAdapter = new ImageListAdapter(mContext, searchTemplate, AddPicSearchTemplateDialog.this);
                search_template_recycler.setLayoutManager(new GridLayoutManager(mContext, 2));
                search_template_recycler.setAdapter(imageListAdapter);
            }
        });
    }

    private void setSearchTemplateEditTextListeners() {
        searchTemplateEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
                    setImages(images);
                }
            }
        });
    }



    @OnClick(R.id.choose_gallery_btn) void fromGallery()
    {
        addPicCallback.chooseTemplate();
    }

    @OnClick(R.id.closeDialog) void dismiss()
    {
        dismisDialog();
    }


    public  void showAddPicDialog()
    {
        addPicdialog.show();
    }
    public void dismisDialog()
    {
        addPicdialog.dismiss();
    }

    @Override
    public void onViewClick(String imageUrl) {
        addPicCallback.setPicToView(imageUrl);
    }

    @Override
    public void onTemplateClicked(String imageUrl) {
        addPicCallback.setPicToView(imageUrl);
    }

    public void setImages(Images[] images) {
        templatesAdapter = new TemplatesAdapter(mContext, images, this);
        search_template_recycler.setAdapter(templatesAdapter);
        search_template_recycler.setLayoutManager(new GridLayoutManager(mContext, 2));
//        imageNames = new String[images.length];
//        for (int i = 0; i < images.length; i++) {
//            imageNames[i] = images[i].name;
//        }
//        ArrayAdapter<String> adapter=new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, imageNames);
//        searchTemplateEdit.setThreshold(1);
//        searchTemplateEdit.setAdapter(adapter);
        this.images = images;
    }

    private void setSearchTemplateEditEditorListener() {
        searchTemplateEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!searchTemplateEdit.getText().toString().trim().isEmpty()) {
                        searchTemplate = new ArrayList<>();
                        for (int i = 0; i < images.length; i++) {
                            if (images[i].name.contains(searchTemplateEdit.getText().toString())) {
                                searchTemplate.add(new Template_class(images[i].id, images[i].name, images[i].image));
                            }
                        }
                        if (searchTemplate.size() == 0) {
                            Toast.makeText(mContext, "No Templates Found", Toast.LENGTH_SHORT).show();
                        } else {
                            imageListAdapter = new ImageListAdapter(mContext, searchTemplate, AddPicSearchTemplateDialog.this);
                            search_template_recycler.setLayoutManager(new GridLayoutManager(mContext, 2));
                            search_template_recycler.setAdapter(imageListAdapter);
                        }
                    } else {
                        Toast.makeText(mContext, "Please Enter Text", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                return false;
            }
        });
    }

}
