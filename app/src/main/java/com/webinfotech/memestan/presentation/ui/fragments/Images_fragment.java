package com.webinfotech.memestan.presentation.ui.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webinfotech.memestan.R;
import com.webinfotech.memestan.domain.models.Template_class;
import com.webinfotech.memestan.presentation.ui.adapters.LocalFilesAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class Images_fragment extends Fragment implements LocalFilesAdapter.Callback {

    public interface Callback
    {
        void myImagesCallback(String imageUrl);
        void showBottomNavImages();
        void hideBottomNavImages();
    }

    View view;
    @BindView(R.id.recyclerview_home)
    RecyclerView myImagesRecyclerView;
    LocalFilesAdapter myAdapter;
    ArrayList<Template_class> myImagesList;
    Callback myImgCallback;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_images_fragment, container, false);
        ButterKnife.bind(this,view);
        fetchFiles();
        myAdapter=new LocalFilesAdapter(getContext(),myImagesList,this);
        myImagesRecyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        myImagesRecyclerView.setAdapter(myAdapter);
        myImagesRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dx<dy)
                {
                    myImgCallback.showBottomNavImages();

                }
                if (dy<dx)
                {
                    myImgCallback.hideBottomNavImages();

                }
            }
        });


        return view;
    }

    private void fetchFiles()
    {
        myImagesList = new ArrayList<>();
        String path = Environment.getExternalStorageDirectory().toString()+"/Meme/Images";
        File directory = new File(path);
        if (directory.exists()) {
            File[] files = directory.listFiles();
            if (files !=null && files.length>0)
            {
                Arrays.sort( files, new Comparator()
                {
                    public int compare(Object o1, Object o2) {

                        if (((File)o1).lastModified() > ((File)o2).lastModified()) {
                            return -1;
                        } else if (((File)o1).lastModified() < ((File)o2).lastModified()) {
                            return +1;
                        } else {
                            return 0;
                        }
                    }

                });
                for (int i =0; i < files.length; i++)
                {
                    myImagesList.add(new Template_class(i,"",files[i].getAbsolutePath()));
                }

            }

        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof Callback)
        {
            myImgCallback=(Callback)context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        myImgCallback=null;
    }

    @Override
    public void onViewClick(String imageUrl) {
        myImgCallback.myImagesCallback(imageUrl);
    }


}
