package com.webinfotech.memestan.presentation.ui.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webinfotech.memestan.R;
import com.webinfotech.memestan.domain.models.Text_color_class;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageCaptionColorAdapter extends RecyclerView.Adapter<ImageCaptionColorAdapter.ViewHolder> {

    public interface Callback {
        void onCaptionColorClicked(int id);
    }

    Context mContext;
    List<Text_color_class> colors;
    Callback mCallback;

    public ImageCaptionColorAdapter(Context mContext, List<Text_color_class> colors, Callback mCallback) {
        this.mContext = mContext;
        this.colors = colors;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater=LayoutInflater.from(mContext);
        view=mInflater.inflate(R.layout.custom_layout_text_color,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Drawable background_drawable=holder.textColorView.getBackground();
        GradientDrawable gradientDrawable=(GradientDrawable)background_drawable;
        gradientDrawable.setColor(android.graphics.Color.parseColor(colors.get(position).hex_code));

        if (colors.get(position).isSelected)
        {
            holder.backgroundView.setVisibility(View.VISIBLE);
        }
        else {
            holder.backgroundView.setVisibility(View.GONE);
        }

        holder.textColorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onCaptionColorClicked(colors.get(position).text_id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return colors.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.backgroud_text_colour)
        View backgroundView;
        @BindView(R.id.text_color)
        View textColorView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    }

    public void updateDataSet(List<Text_color_class> colors)
    {
        this.colors=colors;
        notifyDataSetChanged();
    }

}
