package com.webinfotech.memestan.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.material.card.MaterialCardView;
import com.webinfotech.memestan.R;
import com.webinfotech.memestan.domain.models.Images;
import com.webinfotech.memestan.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TemplatesAdapter extends RecyclerView.Adapter<TemplatesAdapter.ViewHolder> {

    public interface Callback
    {
        void onTemplateClicked(String imageUrl);
    }

    Context mContext;
    Images[] templates;
    Callback mCallback;

    public TemplatesAdapter(Context mContext, Images[] templates, Callback callback) {
        this.mContext = mContext;
        this.templates = templates;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater=LayoutInflater.from(parent.getContext());
        view=mInflater.inflate(R.layout.cardview_item_templates,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageView(mContext, holder.grid_imageView, mContext.getResources().getString(R.string.base_url) + "template/" + templates[position].image);
        holder.grid_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onTemplateClicked(mContext.getResources().getString(R.string.base_url) + "template/" + templates[position].image);
            }
        });
    }

    @Override
    public int getItemCount() {
        return templates.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.cardview_image)
        ImageView grid_imageView;
        @BindView(R.id.card_grid_image)
        MaterialCardView grid_card;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
