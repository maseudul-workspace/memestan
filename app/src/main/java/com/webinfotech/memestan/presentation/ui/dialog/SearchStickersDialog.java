package com.webinfotech.memestan.presentation.ui.dialog;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.memestan.R;
import com.webinfotech.memestan.domain.models.Images;
import com.webinfotech.memestan.domain.models.Template_class;
import com.webinfotech.memestan.presentation.ui.adapters.ImageListAdapter;
import com.webinfotech.memestan.presentation.ui.adapters.StickersAdapter;
import com.webinfotech.memestan.presentation.ui.adapters.Stickers_recycler_adapter;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchStickersDialog implements StickersAdapter.Callback, Stickers_recycler_adapter.Callback {

    public interface Callback
    {
        void setStickerToView(String url);
        void chooseSticker();
    }

    Callback stickersCallback;
    Activity mActivity;
    Context mContext;
    View stickerDialogContainer;
    AlertDialog.Builder stickerBuilder;
    AlertDialog stickerDialog;
    @BindView(R.id.search_template_recyclerview)
    RecyclerView search_template_recycler;
    @BindView(R.id.edt_txt_search_view)
    AutoCompleteTextView searchTemplateEdit;
    ArrayList<Template_class> searchTemplate;
    Stickers_recycler_adapter imageListAdapter;
    StickersAdapter templatesAdapter;
    String[] imageNames;
    Images[] images;

    public SearchStickersDialog(Callback stickersCallback, Activity mActivity, Context mContext) {
        this.stickersCallback = stickersCallback;
        this.mActivity = mActivity;
        this.mContext = mContext;
    }

    public void setUpDialog()
    {
        stickerDialogContainer=mActivity.getLayoutInflater().inflate(R.layout.add_pic_search_dialog,null);
        stickerBuilder=new AlertDialog.Builder(mContext,android.R.style.Theme_DeviceDefault_NoActionBar_Fullscreen);
        stickerBuilder.setView(stickerDialogContainer);
        stickerDialog=stickerBuilder.create();
        ButterKnife.bind(this,stickerDialogContainer);
        setSearchTemplateEditTextListeners();
        setSearchTemplateEditEditorListener();
    }

    @OnClick(R.id.choose_gallery_btn) void fromGallery()
    {
        stickersCallback.chooseSticker();
    }

    private void setSearchTemplateEditTextListeners()
    {
        searchTemplateEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
                    setImages(images);
                }
            }
        });
    }

    public void showSearchSrickerDialog()
    {
        stickerDialog.show();
    }

    public  void dismisDialog()
    {
        stickerDialog.dismiss();
    }

    @OnClick(R.id.closeDialog) void dismiss()
    {
        dismisDialog();
    }

    public void setImages(Images[] images) {
        templatesAdapter = new StickersAdapter(mContext, images, this);
        search_template_recycler.setAdapter(templatesAdapter);
        search_template_recycler.setLayoutManager(new GridLayoutManager(mContext, 2));
//        imageNames = new String[images.length];
//        for (int i = 0; i < images.length; i++) {
//            imageNames[i] = images[i].name;
//        }
//        ArrayAdapter<String> adapter=new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, imageNames);
//        searchTemplateEdit.setThreshold(1);
//        searchTemplateEdit.setAdapter(adapter);
        this.images = images;
    }

    @Override
    public void onStickersClicked(String imageUrl) {
        stickersCallback.setStickerToView(imageUrl);
    }

    private void setSearchTemplateEditEditorListener() {
        searchTemplateEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!searchTemplateEdit.getText().toString().trim().isEmpty()) {
                        searchTemplate = new ArrayList<>();
                        for (int i = 0; i < images.length; i++) {
                            if (images[i].name.contains(searchTemplateEdit.getText().toString())) {
                                searchTemplate.add(new Template_class(images[i].id, images[i].name, images[i].image));
                            }
                        }
                        if (searchTemplate.size() == 0) {
                            Toast.makeText(mContext, "No Templates Found", Toast.LENGTH_SHORT).show();
                        } else {
                            imageListAdapter = new Stickers_recycler_adapter(mContext, searchTemplate, SearchStickersDialog.this);
                            search_template_recycler.setLayoutManager(new GridLayoutManager(mContext, 2));
                            search_template_recycler.setAdapter(imageListAdapter);
                        }
                    } else {
                        Toast.makeText(mContext, "Please Enter Text", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                return false;
            }
        });
    }

}

