package com.webinfotech.memestan.presentation.ui.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.media.Image;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.memestan.R;
import com.webinfotech.memestan.domain.models.Images;
import com.webinfotech.memestan.domain.models.Template_class;
import com.webinfotech.memestan.presentation.ui.adapters.ImageListAdapter;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchTemplateDialog implements ImageListAdapter.Callback {


    public interface Callback {
        void onSearchTemplateClicked(String imageUrl);
    }

    Callback callback;
    Context mContext;
    Activity mActivity;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    @BindView(R.id.search_template_recyclerview)
    RecyclerView search_template_recycler;
    @BindView(R.id.edt_txt_search_view)
    AutoCompleteTextView searchEditText;
    ImageListAdapter templatesAdapter;
    ArrayList<Template_class> searchImages;
    Images[] images;
    String[] templateNames;

    public SearchTemplateDialog(Callback callback, Context mContext, Activity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.callback = callback;
    }

    public void setUpDialog()
    {
        dialogContainer=mActivity.getLayoutInflater().inflate(R.layout.search_template_dialog_layout,null);
        builder=new AlertDialog.Builder(mContext,android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog=builder.create();
        ButterKnife.bind(this,dialogContainer);
        search_template_recycler.setLayoutManager(new GridLayoutManager(mContext,2));
        setSearchTemplateEditEditorListener();
    }
    @OnClick(R.id.closeDialog) void dismis()
    {
        dialog.dismiss();
    }

    private void setAutocompleteListener() {
        searchEditText.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

            }
        });
    }

    public  void showDialog()
    {
        dialog.show();
    }
    public void dismisDialog()
    {
        dialog.dismiss();
    }

    public void setIemplates(Images[] images) {
//        templateNames = new String[images.length];
//        for (int i = 0; i < images.length; i++) {
//            templateNames[i] = images[i].name;
//        }
//        ArrayAdapter<String> adapter=new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, templateNames);
//        searchEditText.setThreshold(1);
//        searchEditText.setAdapter(adapter);
        this.images = images;
    }

    @Override
    public void onViewClick(String imageUrl) {
        dismisDialog();
        callback.onSearchTemplateClicked(imageUrl);
    }

    private void setSearchTemplateEditEditorListener() {
        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!searchEditText.getText().toString().trim().isEmpty()) {
                        searchImages = new ArrayList<>();
                        for (int i = 0; i < images.length; i++) {
                            if (images[i].name.contains(searchEditText.getText().toString())) {
                                searchImages.add(new Template_class(images[i].id, images[i].name, images[i].image));
                            }
                        }
                        if (searchImages.size() == 0) {
                            search_template_recycler.setVisibility(View.INVISIBLE);
                            Toast.makeText(mContext, "No Templates Found", Toast.LENGTH_SHORT).show();
                        } else {
                            search_template_recycler.setVisibility(View.VISIBLE);
                            templatesAdapter = new ImageListAdapter(mContext, searchImages, SearchTemplateDialog.this);
                            search_template_recycler.setLayoutManager(new GridLayoutManager(mContext, 2));
                            search_template_recycler.setAdapter(templatesAdapter);
                        }
                    } else {
                        Toast.makeText(mContext, "Please Enter Text", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                return false;
            }
        });
    }

}
