package com.webinfotech.memestan.presentation.ui.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webinfotech.memestan.R;
import com.webinfotech.memestan.domain.models.Template_class;
import com.webinfotech.memestan.presentation.ui.adapters.LocalFilesAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class Videos_fragment extends Fragment implements LocalFilesAdapter.Callback {

    public interface Callback
    {
        void myVideosCallback(String videoUrl);
        void showBottomNavVideos();
        void hideBottomNavVideos();
    }

    View view;
    @BindView(R.id.recyclerview_home)
    RecyclerView myVideosRecyclerView;
    LocalFilesAdapter myAdapter;
    ArrayList<Template_class> myVideosList;
    Callback myVidCallback;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_images_fragment, container, false);
        ButterKnife.bind(this,view);
        fetchFiles();
        myAdapter=new LocalFilesAdapter(getContext(),myVideosList,this);
        myVideosRecyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        myVideosRecyclerView.setAdapter(myAdapter);
        myVideosRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dx<dy)
                {
                    myVidCallback.showBottomNavVideos();

                }
                if (dy<dx)
                {
                    myVidCallback.hideBottomNavVideos();

                }
            }
        });
        return view;
    }

    private void fetchFiles() {
        myVideosList = new ArrayList<>();
        String path = Environment.getExternalStorageDirectory().toString() + "/Meme/Videos";
        File directory = new File(path);
        if (directory.exists()) {
            File[] files = directory.listFiles();
            if (files != null && files.length > 0) {
                Arrays.sort(files, new Comparator() {
                    public int compare(Object o1, Object o2) {

                        if (((File) o1).lastModified() > ((File) o2).lastModified()) {
                            return -1;
                        } else if (((File) o1).lastModified() < ((File) o2).lastModified()) {
                            return +1;
                        } else {
                            return 0;
                        }
                    }

                });
                for (int i = 0; i < files.length; i++) {
                    myVideosList.add(new Template_class(i, "", files[i].getAbsolutePath()));
                }

            }

        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof Callback)
        {
            myVidCallback=(Callback)context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        myVidCallback=null;
    }

    @Override
    public void onViewClick(String imageUrl) {
        myVidCallback.myVideosCallback(imageUrl);
    }

}
