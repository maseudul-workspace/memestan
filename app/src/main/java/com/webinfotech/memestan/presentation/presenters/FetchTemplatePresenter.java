package com.webinfotech.memestan.presentation.presenters;

import com.webinfotech.memestan.domain.models.Images;
import com.webinfotech.memestan.presentation.ui.adapters.StickersAdapter;
import com.webinfotech.memestan.presentation.ui.adapters.TemplatesAdapter;

public interface FetchTemplatePresenter {
    void fetchTemplates();
    void fetchStickers();
    interface View {
        void loadTemplatesAdapter(TemplatesAdapter adapter, Images[] images);
        void loadStickersAdapters(StickersAdapter stickersAdapter, Images[] images);
        void onTemplateClicked(String imageUrl);
    }
}
