package com.webinfotech.memestan.presentation.presenters.base;

import com.webinfotech.memestan.domain.executors.Executor;
import com.webinfotech.memestan.domain.executors.MainThread;

/**
 * Created by Raj on 05-01-2019.
 */

public abstract class AbstractPresenter {
    protected Executor mExecutor;
    protected MainThread mMainThread;
    public AbstractPresenter(Executor executor, MainThread mainThread){
        this.mExecutor = executor;
        this.mMainThread = mainThread;
    }
}
