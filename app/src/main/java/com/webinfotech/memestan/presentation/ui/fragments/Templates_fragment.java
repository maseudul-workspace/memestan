package com.webinfotech.memestan.presentation.ui.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.webinfotech.memestan.R;
import com.webinfotech.memestan.domain.executors.impl.ThreadExecutor;
import com.webinfotech.memestan.domain.models.Images;
import com.webinfotech.memestan.presentation.presenters.FetchTemplatePresenter;
import com.webinfotech.memestan.presentation.presenters.impl.FetchTemplatesPresenterImpl;
import com.webinfotech.memestan.presentation.ui.adapters.StickersAdapter;
import com.webinfotech.memestan.presentation.ui.adapters.TemplatesAdapter;
import com.webinfotech.memestan.threading.MainThreadImpl;

import java.util.ArrayList;


import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class Templates_fragment extends Fragment implements FetchTemplatePresenter.View {


    public interface Callback
    {
        void showBottomNav();
        void hideBottomNav();
        void onTemplateClicked(String imageUrl);
        void showDialogClicked();
        void setDialogTemplates(Images[] images);
    }

    View view;
    @BindView(R.id.recyclerview_home)
    RecyclerView recyclerViewTemplates;
    Callback callback;
    @BindView(R.id.search_relative_layout)
    RelativeLayout relativeLayout;
    @BindView(R.id.nestedView)
    NestedScrollView nestedScrollView;
    FetchTemplatePresenter mPresenter;
    Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_templates,container,false);
        ButterKnife.bind(this, view);
       nestedScrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
           @Override
           public void onScrollChange(View view, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

               if (scrollY > oldScrollY) {
                   callback.showBottomNav();
               }
               if (scrollY < oldScrollY) {

                   callback.hideBottomNav();
               }

           }
       });
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.showDialogClicked();
            }
        });
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof Callback) {
            this.mContext = context;
            callback = (Callback) context;
            initialisePresenter(context);
            mPresenter.fetchTemplates();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    public void initialisePresenter(Context context) {
        mPresenter = new FetchTemplatesPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), context, this);
    }

    @Override
    public void loadTemplatesAdapter(TemplatesAdapter adapter, Images[] images) {
        recyclerViewTemplates.setAdapter(adapter);
        recyclerViewTemplates.setLayoutManager(new GridLayoutManager(mContext, 2));
        callback.setDialogTemplates(images);
    }

    @Override
    public void loadStickersAdapters(StickersAdapter stickersAdapter, Images[] images) {

    }

    @Override
    public void onTemplateClicked(String imageUrl) {
        callback.onTemplateClicked(imageUrl);
    }

}
