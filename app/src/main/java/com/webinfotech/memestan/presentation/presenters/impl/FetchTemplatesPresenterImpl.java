package com.webinfotech.memestan.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.memestan.AndroidApplication;
import com.webinfotech.memestan.domain.executors.Executor;
import com.webinfotech.memestan.domain.executors.MainThread;
import com.webinfotech.memestan.domain.interactors.GetStickersInteractor;
import com.webinfotech.memestan.domain.interactors.GetTemplatesInteractor;
import com.webinfotech.memestan.domain.interactors.impl.GetStickersInteractorImpl;
import com.webinfotech.memestan.domain.interactors.impl.GetTemplatesInteractorImpl;
import com.webinfotech.memestan.domain.models.Images;
import com.webinfotech.memestan.presentation.presenters.FetchTemplatePresenter;
import com.webinfotech.memestan.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.memestan.presentation.ui.adapters.StickersAdapter;
import com.webinfotech.memestan.presentation.ui.adapters.TemplatesAdapter;
import com.webinfotech.memestan.repository.AppRepositoryImpl;

public class FetchTemplatesPresenterImpl extends AbstractPresenter implements FetchTemplatePresenter, GetTemplatesInteractor.Callback, GetStickersInteractor.Callback,  TemplatesAdapter.Callback, StickersAdapter.Callback {

    Context mContext;
    FetchTemplatePresenter.View mView;
    GetTemplatesInteractorImpl getTemplatesInteractor;
    GetStickersInteractorImpl getStickersInteractor;
    Images[] templates;
    Images[] stickers;
    AndroidApplication androidApplication;

    public FetchTemplatesPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchTemplates() {
        getTemplatesInteractor = new GetTemplatesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        getTemplatesInteractor.execute();
    }

    @Override
    public void fetchStickers() {
        getStickersInteractor = new GetStickersInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        getStickersInteractor.execute();
    }

    @Override
    public void onFetchingTemplatesSuccess(Images[] templates) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        if (templates.length == 0) {
            if (androidApplication.getTemplates(mContext) != null) {
                this.templates = androidApplication.getTemplates(mContext);
                TemplatesAdapter templatesAdapter = new TemplatesAdapter(mContext, templates, this);
                mView.loadTemplatesAdapter(templatesAdapter, templates);
            }
        } else {
            androidApplication.setTemplates(mContext, templates);
            this.templates = templates;
            TemplatesAdapter templatesAdapter = new TemplatesAdapter(mContext, templates, this);
            mView.loadTemplatesAdapter(templatesAdapter, templates);
        }
    }

    @Override
    public void onFetchingTemplatesFail(String errorMsg) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        if (androidApplication.getTemplates(mContext) != null) {
            this.templates = androidApplication.getTemplates(mContext);
            TemplatesAdapter templatesAdapter = new TemplatesAdapter(mContext, templates, this);
            mView.loadTemplatesAdapter(templatesAdapter, templates);
        }
    }

    @Override
    public void onGettingStickersSuccess(Images[] images) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        if (images.length == 0) {
            if (androidApplication.getStickers(mContext) != null) {
                this.stickers = androidApplication.getStickers(mContext);
                StickersAdapter stickersAdapter = new StickersAdapter(mContext, this.stickers, this);
                mView.loadStickersAdapters(stickersAdapter, this.stickers);
            }
        } else {
            androidApplication.setStickers(mContext, images);
            this.stickers = images;
            StickersAdapter stickersAdapter = new StickersAdapter(mContext, images, this);
            mView.loadStickersAdapters(stickersAdapter, images);
        }
    }

    @Override
    public void onGettingStickersFail(String errorMsg) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        if (androidApplication.getStickers(mContext) != null) {
            this.stickers = androidApplication.getStickers(mContext);
            StickersAdapter stickersAdapter = new StickersAdapter(mContext, this.stickers, this);
            mView.loadStickersAdapters(stickersAdapter, this.stickers);
        }
    }

    @Override
    public void onTemplateClicked(String imageUrl) {
        mView.onTemplateClicked(imageUrl);
    }

    @Override
    public void onStickersClicked(String imageUrl) {

    }
}
