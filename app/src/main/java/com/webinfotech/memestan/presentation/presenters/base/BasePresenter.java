package com.webinfotech.memestan.presentation.presenters.base;

/**
 * Created by Raj on 05-01-2019.
 */

public interface BasePresenter {
      /* Method that control the lifecycle of the view. It should be called in the view's
            * (Activity or Fragment) onResume() method.
            */
    void resume();

    /**
     * Method that controls the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onPause() method.
     */
    void pause();

    /**
     * Method that controls the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onStop() method.
     */
    void stop();

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onDestroy() method.
     */
    void destroy();


    /**
     * Method that should signal the appropriate view to show the appropriate error with the provided message.
     */
}
