package com.webinfotech.memestan.presentation.ui.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.webinfotech.memestan.R;
import com.webinfotech.memestan.domain.models.Text_color_class;
import com.webinfotech.memestan.presentation.ui.adapters.CaptionBgColorAdapter;
import com.webinfotech.memestan.presentation.ui.adapters.CaptionTextColorAdapter;
import com.webinfotech.memestan.threading.FfmpegThreading;
import com.webinfotech.memestan.util.Helper;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class VideoEditActivity extends AppCompatActivity implements FfmpegThreading.Callback, CaptionBgColorAdapter.Callback, CaptionTextColorAdapter.Callback
{

    @BindView(R.id.exoplayer_playerview)
    PlayerView playerView;
    SimpleExoPlayer exoplayer;
    @BindView(R.id.layout_trim)
    View layoutTrim;
    @BindView(R.id.range_seek_bar)
    RangeSeekBar rangeSeekBar;
    @BindView(R.id.txt_view_start_time)
    TextView txtViewStartTime;
    @BindView(R.id.txt_view_end_time)
    TextView txtViewEndTime;
    @BindView(R.id.recycler_view_caption_bg_color)
    RecyclerView recyclerViewCaptionBgColor;
    @BindView(R.id.recycler_view_caption_text_color)
    RecyclerView recyclerViewCaptionTextColor;
    @BindView(R.id.layout_caption1)
    View layoutCaption1;
    @BindView(R.id.layout_caption)
    View layoutCaption;
    @BindView(R.id.txt_view_caption1)
    TextView txtViewCaption1;
    @BindView(R.id.edit_text_caption)
    EditText editTextCaption;
    @BindView(R.id.video_feature)
    View videoFeature;
    List<Text_color_class> captionTextColors;
    List<Text_color_class> captionBgColors;
    long duration;
    FfmpegThreading ffmpegThreading;
    String[] command;
    ProgressDialog progressDialog;
    boolean isFirstPlay = true;
    DataSource.Factory dataSourceFactory;
    CaptionBgColorAdapter captionBgColorAdapter;
    CaptionTextColorAdapter captionTextColorAdapter;
    int fontSize;
    int padding;
    String text;
    String outputFile = System.currentTimeMillis() + ".mp4";
    String textColor = "#000000";
    String paddingBgColor = "#FFFFFF";
    @BindView(R.id.adView)
    AdView mAdView;
    AlertDialog.Builder builder;
    InterstitialAd interstitialAd;
    @BindView(R.id.back_arrow)
    ImageView backArrow;
    @BindView(R.id.save)
    ImageView save_video;
    @BindView(R.id.radio_group_caption_text_alignment)
    RadioGroup radioGroupTextAlignment;
    String inputPath;
    String outputPath;
    boolean selfCalling;
    boolean isFromGallery;
    String captionAlign = "5";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_edit);
        ButterKnife.bind(this);
        inputPath = getIntent().getStringExtra("videoUri");
        isFromGallery = getIntent().getBooleanExtra("isFromGallery", false);
        selfCalling = getIntent().getBooleanExtra("selfCalling", false);
        builder=new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        if (isFromGallery) {
            inputPath = Helper.getRealPathFromUri(this, Uri.parse(getIntent().getStringExtra("videoUri")));
        }

        initialiseExoplayer();
        setUpProgressDialog();
        setColorsRecyclerView();
        checkVideoWidth();
        // banner ads preparation
        bannerAds();
        // prepare interstitial ads
        interstitialAds();
        setOutputPath();
        setRadioGroupTextAlignment();
    }

    public void setRadioGroupTextAlignment() {
        radioGroupTextAlignment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_btn_center:
                        captionAlign = "(w-tw)/2";
                        break;
                    case R.id.radio_btn_left:
                        captionAlign = "5";
                        break;
                }
            }
        });
    }

    private void interstitialAds() {
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-4687730962574001/1976565846");
        interstitialAd.loadAd(new AdRequest.Builder().build());
    }

    private void bannerAds() {
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-4687730962574001/2359709222");
    }

    public void checkVideoWidth() {
        int videoWidth = Helper.getVideoWidth(inputPath);
        if (videoWidth < 300) {
            fontSize = 12;
            padding = 25;
        } else if (300 < videoWidth && videoWidth < 450) {
            fontSize = 18;
            padding = 35;
        } else if (450 < videoWidth && videoWidth < 650) {
            fontSize = 26;
            padding = 45;
        } else if (650 < videoWidth && videoWidth < 750) {
            fontSize = 45;
            padding = 50;
        } else if (750 < videoWidth && videoWidth < 1250) {
            fontSize = 55;
            padding = 65;
        } else {
            fontSize = 70;
            padding = 85;
        }
    }

    public void initialiseExoplayer() {
        exoplayer = ExoPlayerFactory.newSimpleInstance(this);
        playerView.setPlayer(exoplayer);
        dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "meme"));
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(inputPath));
        exoplayer.prepare(videoSource);
        exoplayer.setPlayWhenReady(true);
        exoplayer.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playWhenReady && isFirstPlay) {
                    duration = exoplayer.getDuration();
                    duration = duration/1000;
                    rangeSeekBar.setRangeValues(0,duration);
                    rangeSeekBar.setSelectedMaxValue(duration);
                    rangeSeekBar.setSelectedMinValue(0);
                    txtViewStartTime.setText("Start Time: 00:00:00");
                    txtViewEndTime.setText("End Time: " + getTime(duration));
                    rangeSeekBar.setEnabled(true);
                    rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
                        @Override
                        public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object maxValue) {
                            txtViewStartTime.setText("Start Time: " + getTime((int)bar.getSelectedMinValue()));
                            txtViewEndTime.setText("End Time: " + getTime((int)bar.getSelectedMaxValue()));
                        }
                    });
                    isFirstPlay = false;
                }
            }
        });
    }

    public void setColorsRecyclerView() {

        captionBgColors=new ArrayList<>();
        captionBgColors.add(new Text_color_class(1,"#333333",false));
        captionBgColors.add(new Text_color_class(2,"#e4bc04",false));
        captionBgColors.add(new Text_color_class(3,"#b5c1e6",false));
        captionBgColors.add(new Text_color_class(4,"#ff4500",false));
        captionBgColors.add(new Text_color_class(5,"#ffc500",false));
        captionBgColors.add(new Text_color_class(6,"#71acda",false));
        captionBgColors.add(new Text_color_class(7,"#c4a857",false));
        captionBgColors.add(new Text_color_class(8,"#7157c4",false));
        captionBgColors.add(new Text_color_class(9,"#b269ff",false));
        captionBgColors.add(new Text_color_class(10,"#e6e6fa",false));
        captionBgColors.add(new Text_color_class(11,"#cc0000",false));
        captionBgColors.add(new Text_color_class(12,"#488e96",false));

        captionTextColors=new ArrayList<>();
        captionTextColors.add(new Text_color_class(1,"#333333",false));
        captionTextColors.add(new Text_color_class(2,"#e4bc04",false));
        captionTextColors.add(new Text_color_class(3,"#b5c1e6",false));
        captionTextColors.add(new Text_color_class(4,"#ff4500",false));
        captionTextColors.add(new Text_color_class(5,"#ffc500",false));
        captionTextColors.add(new Text_color_class(6,"#71acda",false));
        captionTextColors.add(new Text_color_class(7,"#c4a857",false));
        captionTextColors.add(new Text_color_class(8,"#7157c4",false));
        captionTextColors.add(new Text_color_class(9,"#b269ff",false));
        captionTextColors.add(new Text_color_class(10,"#e6e6fa",false));
        captionTextColors.add(new Text_color_class(11,"#cc0000",false));
        captionTextColors.add(new Text_color_class(12,"#488e96",false));

        captionBgColorAdapter = new CaptionBgColorAdapter(this, captionBgColors, this);
        recyclerViewCaptionBgColor.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewCaptionBgColor.setAdapter(captionBgColorAdapter);
        captionTextColorAdapter = new CaptionTextColorAdapter(this, captionTextColors, this);
        recyclerViewCaptionTextColor.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewCaptionTextColor.setAdapter(captionTextColorAdapter);
    }

    public void setOutputPath() {
        File folder=new File(Environment.getExternalStorageDirectory()+"/Meme/Videos/");
        if (!folder.exists())
        {
            folder.mkdirs();
        }

        File dest = new File(folder, outputFile);
        outputPath = dest.getAbsolutePath();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        exoplayer.release();
    }

    @OnClick(R.id.layout_trim_option) void onLayoutTrimOptionClicked() {
        layoutTrim.setVisibility(View.VISIBLE);
        layoutCaption.setVisibility(View.GONE);
        layoutCaption1.setVisibility(View.GONE);
        videoFeature.setVisibility(View.GONE);
    }

    @OnClick(R.id.layout_caption_option) void onLayoutCaptionOptionClicked() {
        layoutCaption.setVisibility(View.VISIBLE);
        layoutCaption1.setVisibility(View.VISIBLE);
        layoutTrim.setVisibility(View.GONE);
        videoFeature.setVisibility(View.GONE);
    }

    private String getTime(long seconds)
    {
        long hr=seconds/3600;
        long rem=seconds % 3600;
        long mn=rem/60;
        long sec=rem % 60;
        return  String.format("%02d",hr)+":"+String.format("%02d",mn)+":"+String.format("%02d",sec);
    }

    @OnClick(R.id.img_save_trim_video) void onSaveTrimVideo() {
        videoFeature.setVisibility(View.VISIBLE);
        int endMs = rangeSeekBar.getSelectedMaxValue().intValue()*1000;
        int startMs = rangeSeekBar.getSelectedMinValue().intValue()*1000;
        command=new String[]{"-ss",""+startMs/1000,"-y","-i",inputPath,"-t",""+(endMs-startMs)/1000,"-vcodec","mpeg4","-b:v","2097152","-b:a","48000","-ac","2","-ar","22050",outputPath};
        ffmpegThreading = new FfmpegThreading(this, command,(endMs-startMs)/1000, this);
        ffmpegThreading.start();
    }

    @OnClick(R.id.img_view_cancel_trim_layout) void onCancelTrimLayoutClicked() {
        layoutTrim.setVisibility(View.GONE);
        videoFeature.setVisibility(View.VISIBLE);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setCancelable(false);
        progressDialog.setMax(100);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ffmpegThreading.cancelTask();
                progressDialog.dismiss();//dismiss dialog
            }
        });
    }

    @Override
    public void onProgressChanged(int progress) {
        if (progress <= 100) {
            progressDialog.setProgress(progress);
            progressDialog.setMessage("Please Wait... " + progress + "%");
            progressDialog.show();
        }
    }

    @Override
    public void onTaskFinish() {
        ffmpegThreading.cancelTask();
        if (selfCalling) {
            File file = new File(inputPath);
            file.delete();
        }
        progressDialog.dismiss();
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(Uri.fromFile(new File(outputPath)));
        sendBroadcast(intent);
        Intent myvideo=new Intent(this, VideoEditActivity.class);
        myvideo.putExtra("videoUri",outputPath);
        myvideo.putExtra("selfCalling",true);
        startActivity(myvideo);
        finish();
    }

    @Override
    public void onCaptionBgColorClicked(int id) {
        for (int i = 0; i < captionBgColors.size(); i++) {
            if (captionBgColors.get(i).text_id == id) {
                captionBgColors.get(i).isSelected = true;
                layoutCaption1.setBackgroundColor(Color.parseColor(captionBgColors.get(i).hex_code));
                paddingBgColor = captionBgColors.get(i).hex_code;
            } else {
                captionBgColors.get(i).isSelected = false;
            }
        }
        captionBgColorAdapter.updateDataSet(captionBgColors);
    }

    @Override
    public void onCaptionTextColorClicked(int id) {
        for (int i = 0; i < captionTextColors.size(); i++) {
            if (captionTextColors.get(i).text_id == id) {
                captionTextColors.get(i).isSelected = true;
                txtViewCaption1.setTextColor(Color.parseColor(captionTextColors.get(i).hex_code));
                textColor = captionTextColors.get(i).hex_code;
            } else {
                captionTextColors.get(i).isSelected = false;
            }
        }
        captionTextColorAdapter.updateDataSet(captionTextColors);
    }

    @OnClick(R.id.img_view_save_caption) void onSaveCaptionClicked() {

        videoFeature.setVisibility(View.VISIBLE);
        text = "";
//        String[] stringArray = editTextCaption.getText().toString().split("\n");
        int finalPadding = editTextCaption.getLineCount() * padding;

        int x = 45;
        String[] strings = editTextCaption.getText().toString().split(" ");
        for (int i = 0; i < strings.length; i++) {
            if (strings[i].length() == 45) {
                if (text.isEmpty()) {
                    text = strings[i];
                } else {
                    text = text + "\r" + strings[i];
                    x = x + 45;
                }
            } else if (strings[i].length() < 45) {
                if (text.length() + strings[i].length() + 1 > x) {
                    text = text + "\r" + strings[i];
                    x = x + 45;
                } else {
                    text = text + " " + strings[i];
                }
            } else {
                List<String> stringArrayList = new ArrayList<>();
                int index = 0;
                while (index < strings[i].length()) {
                    stringArrayList.add(strings[i].substring(index, Math.min(index + 45,strings[i].length())));
                    index += 45;
                }
                for (int j = 0; j < stringArrayList.size(); j++) {
                    text = text + "\r" + stringArrayList.get(j);
                    x = x + 45;
                }
            }
        }
        text = text.replaceAll("\n", "\r");
        text = text.replaceAll(":", "-");
        saveCaption(finalPadding, text);

    }

    public void saveCaption(int finalPadding, String text) {
        onProgressChanged(0);
        int endMs = rangeSeekBar.getSelectedMaxValue().intValue()*1000;
        int startMs = rangeSeekBar.getSelectedMinValue().intValue()*1000;
        File fontFile =new  File(
                Environment.getExternalStorageDirectory(),
                File.separator + "Meme" + File.separator + ".Font" + File.separator + "Roboto.ttf");
        command = new String[]{"-y","-i", inputPath, "-vf",
                "pad=iw:ih+" + finalPadding + ":0:" + finalPadding + ":" + paddingBgColor + ",drawtext=fontfile= "+fontFile.getAbsolutePath()+":text=" + text + ":fontcolor=" + textColor + ":fontsize=" + fontSize + " :x=" + captionAlign + ":y=10", outputPath};
        ffmpegThreading = new FfmpegThreading(this, command,(endMs-startMs)/1000, this);
        ffmpegThreading.start();
    }

    @OnClick(R.id.img_view_cancel_caption_layout) void captionCancelLayoutClicked() {
        layoutCaption.setVisibility(View.GONE);
        layoutCaption1.setVisibility(View.GONE);
        videoFeature.setVisibility(View.VISIBLE);
    }


    @Override
    public void onBackPressed() {
        builder.setTitle("Confirm Exit");
        builder.setMessage(Html.fromHtml("<font color='#6101e9'>Do you want to save?</font>"));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                } else {
                    interstitialAd.show();
                }
                finish();

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (selfCalling) {
                    File file = new File(inputPath);
                    file.delete();
                }
                File file = new File(outputPath);
                file.delete();
                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                } else {
                    interstitialAd.show();
                }
                finish();
            }
        });

        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    @OnClick(R.id.back_arrow) void setBackArrow()
    {
        builder.setTitle("Confirm Exit");
        builder.setMessage(Html.fromHtml("<font color='#6101e9'>Do you want to save?</font>"));

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                } else {
                    interstitialAd.show();
                }
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                File file = new File(outputPath);
                file.delete();

                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                } else {
                    interstitialAd.show();
                }
                finish();
            }
        });

        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();

    }

    @OnClick(R.id.save)void setSaveVideo()
    {
        Toast.makeText(this, "Edited file saved successfully", Toast.LENGTH_SHORT).show();
    }
    @OnClick(R.id.preview) void showPreview()
    {
        Toast.makeText(this, "Preview", Toast.LENGTH_SHORT).show();
    }
}
