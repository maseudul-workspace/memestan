package com.webinfotech.memestan.presentation.ui.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.watermark.androidwm.WatermarkBuilder;
import com.watermark.androidwm.bean.WatermarkImage;
import com.watermark.androidwm.bean.WatermarkText;
import com.webinfotech.memestan.R;
import com.webinfotech.memestan.domain.executors.impl.ThreadExecutor;
import com.webinfotech.memestan.domain.models.Images;
import com.webinfotech.memestan.domain.models.Template_class;
import com.webinfotech.memestan.domain.models.Text_color_class;
import com.webinfotech.memestan.presentation.presenters.FetchTemplatePresenter;
import com.webinfotech.memestan.presentation.presenters.impl.FetchTemplatesPresenterImpl;
import com.webinfotech.memestan.presentation.ui.adapters.ImageCaptionColorAdapter;
import com.webinfotech.memestan.presentation.ui.adapters.RecyclerView_for_text_colour;
import com.webinfotech.memestan.presentation.ui.adapters.StickersAdapter;
import com.webinfotech.memestan.presentation.ui.adapters.Stickers_recycler_adapter;
import com.webinfotech.memestan.presentation.ui.adapters.TemplatesAdapter;
import com.webinfotech.memestan.presentation.ui.dialog.AddPicSearchTemplateDialog;
import com.webinfotech.memestan.presentation.ui.dialog.SearchStickersDialog;
import com.webinfotech.memestan.threading.MainThreadImpl;
import com.webinfotech.memestan.threading.Templates_Thread;
import com.webinfotech.memestan.util.GlideHelper;
import com.webinfotech.memestan.util.MyDragShadowBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ImageEditActivity extends AppCompatActivity implements RecyclerView_for_text_colour.Callback, ImageCaptionColorAdapter.Callback, SearchStickersDialog.Callback,
        Templates_Thread.Callback, AddPicSearchTemplateDialog.Callback, FetchTemplatePresenter.View {
    // for adding text in image
    Uri imageuri;
    @BindView(R.id.image_editor_layout)
    ImageView imageedit;
    @BindView(R.id.back_arrow)
    ImageView backArrow;
    @BindView(R.id.text_feature)
    ImageView text_feature;
    @BindView(R.id.crop_feature)
    ImageView crop_feature;
    @BindView(R.id.stickers_feature)
    ImageView sticker_feature;
    @BindView(R.id.hide_edit_text)
    ImageView close_edit_text;
    @BindView(R.id.save_text)
    ImageView save_added_text;
    @BindView(R.id.add_text_editor)
    TextView add_Text;
    @BindView(R.id.feature_linear_layout)
    LinearLayout feature_layout;
    @BindView(R.id.image_relative_layout)
    RelativeLayout editing_image_layout;
    @BindView(R.id.text_editing_layout_relative)
    RelativeLayout text_editing_relative_layout;
    @BindView(R.id.dragable_text)
    TextView dragText;
    @BindView(R.id.zoom_edit_text)
    SeekBar zooming_text;
    List<Text_color_class> mTextColour;
    List<Text_color_class> captionColorArray;
    float loc_of_text_at_x,loc_of_text_at_y;
    int color=Color.BLACK;
    RecyclerView_for_text_colour myAdapter;
    int captionColor = Color.BLACK;
    ImageCaptionColorAdapter imageCaptionColorAdapter;
    // for cropping image
    String temp_storage;
    BitmapDrawable drawable;
    Bitmap bitmap;
    // stickers
    @BindView(R.id.stickers_relative_layout)
    LinearLayout stickers_relative;
    @BindView(R.id.zoom_stickers)
    SeekBar zoom_stickers;
    @BindView(R.id.image_stickers_view)
    ImageView add_stickers;
    @BindView(R.id.hide_stickers)
    ImageView hide_stickers;
    @BindView(R.id.save_stickers)
    ImageView save_stickers;
    @BindView(R.id.txt_view_image_caption)
    TextView txtViewImageCaption;
    @BindView(R.id.img_view_caption)
    ImageView imgViewCaption;
    @BindView(R.id.caption_edit_layout)
    View captionEditLayout;
    @BindView(R.id.edit_text_image_caption)
    EditText editTextCaption;
    @BindView(R.id.caption_color_recycler_view)
    RecyclerView captionColorRecyclerView;
    boolean selected_text=true;
    boolean selected_stickers=true;
    AlertDialog.Builder builder;
    int REQUESTCODE_Templates=000;
    int REQUESTCODE_STICKERS=111;
    boolean isFromGallery;
    float dif1_text,diff2_text,diff1_stickers,diff2_stickers;
    int mainLayoutHeigth;
    InterstitialAd interstitialAd;
    // add pic dialog
    AddPicSearchTemplateDialog addPicSearchTemplateDialog;
    // sticker search dialog
    SearchStickersDialog searchStickersDialog;
    ProgressDialog progresstemplatesDownload;
    @BindView(R.id.radio_group_caption_text_alignment)
    RadioGroup radioGroupTextAlignment;
    float prevX = 0;
    float prevY = 0;
    FetchTemplatesPresenterImpl mPresenter;
    int textExtraSize;
    @BindView(R.id.save_caption_layout)
    ImageView saveCaption;
    @BindView(R.id.done)
    TextView doneTxtCaption;
    List<WatermarkText> watermarkTexts;
    double waterMarkYcoordinate = 0.02;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_edit);
        ButterKnife.bind(this);
        isFromGallery = getIntent().getBooleanExtra("isFromGallery", true);
        temp_storage = System.currentTimeMillis() + ".png";
        mainLayoutHeigth = editing_image_layout.getLayoutParams().height;

        // image loading from main activity to ImageEditActivity
        imageuri = Uri.parse(getIntent().getStringExtra("uri"));
        GlideHelper.setImageView(this, imageedit, getIntent().getStringExtra("uri"));
        builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);

        // initialise custom addPicSearchDialog
        initialiseDialog();
        // initialise custom searchStickerDialg
        initialiseSearchStickerDialog();
        setupProgressDialog();
        setListeners();
        setDragListeners();
        setOnTouchListeners();
        arrayListColor();
        setRecyclerView_textColor();
        setRecyclerViewCaptionColor();
        setTextExtraSize();
        // prepare interstitial ads
        interstitialAds();
        // bannerAds

        initialisePresenter();
        mPresenter.fetchStickers();
        mPresenter.fetchTemplates();

    }




    public void initialisePresenter() {
        mPresenter = new FetchTemplatesPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void interstitialAds() {
        // prepare interstitial ads
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-4687730962574001/1976565846");
        interstitialAd.loadAd(new AdRequest.Builder().build());
    }

    public void setupProgressDialog() {
        progresstemplatesDownload=new ProgressDialog(this);
        progresstemplatesDownload.setMessage("Please wait");
        progresstemplatesDownload.setTitle("Downloading");
    }

    private void initialiseSearchStickerDialog()
    {
        searchStickersDialog=new SearchStickersDialog(this,this,this);
        searchStickersDialog.setUpDialog();
    }

    private void initialiseDialog() {
        addPicSearchTemplateDialog=new AddPicSearchTemplateDialog(this,this,this);
        addPicSearchTemplateDialog.setUpDialog();
    }

    private void arrayListColor()
    {
        mTextColour=new ArrayList<>();
        mTextColour.add(new Text_color_class(1,"#333333",false));
        mTextColour.add(new Text_color_class(2,"#e4bc04",false));
        mTextColour.add(new Text_color_class(3,"#b5c1e6",false));
        mTextColour.add(new Text_color_class(4,"#ff4500",false));
        mTextColour.add(new Text_color_class(5,"#ffc500",false));
        mTextColour.add(new Text_color_class(6,"#71acda",false));
        mTextColour.add(new Text_color_class(7,"#c4a857",false));
        mTextColour.add(new Text_color_class(8,"#7157c4",false));
        mTextColour.add(new Text_color_class(9,"#b269ff",false));
        mTextColour.add(new Text_color_class(10,"#e6e6fa",false));
        mTextColour.add(new Text_color_class(11,"#cc0000",false));
        mTextColour.add(new Text_color_class(12,"#488e96",false));

        captionColorArray = new ArrayList<>();
        captionColorArray.add(new Text_color_class(1,"#333333",false));
        captionColorArray.add(new Text_color_class(2,"#e4bc04",false));
        captionColorArray.add(new Text_color_class(3,"#b5c1e6",false));
        captionColorArray.add(new Text_color_class(4,"#ff4500",false));
        captionColorArray.add(new Text_color_class(5,"#ffc500",false));
        captionColorArray.add(new Text_color_class(6,"#71acda",false));
        captionColorArray.add(new Text_color_class(7,"#c4a857",false));
        captionColorArray.add(new Text_color_class(8,"#7157c4",false));
        captionColorArray.add(new Text_color_class(9,"#b269ff",false));
        captionColorArray.add(new Text_color_class(10,"#e6e6fa",false));
        captionColorArray.add(new Text_color_class(11,"#cc0000",false));
        captionColorArray.add(new Text_color_class(12,"#488e96",false));

    }

    private void setRecyclerView_textColor()
    {
        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.text_color_recycler_view);
        myAdapter=new RecyclerView_for_text_colour(this,mTextColour,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false));
        recyclerView.setAdapter(myAdapter);
    }

    private void setRecyclerViewCaptionColor() {
        imageCaptionColorAdapter = new ImageCaptionColorAdapter(this, captionColorArray, this);
        captionColorRecyclerView.setAdapter(imageCaptionColorAdapter);
        captionColorRecyclerView.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false));
    }

    private void setOnTouchListeners()
    {
        dragText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getActionMasked())
                {

                    case MotionEvent.ACTION_DOWN:
                        ClipData data=ClipData.newPlainText("","");
                        View.DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(null);
                        view.startDrag(data,new MyDragShadowBuilder(),view,0);
                        view.setVisibility(View.GONE);
                        dragText.startDrag(data,new MyDragShadowBuilder(),dragText,0);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                }
                return true;
            }
        });

        add_stickers.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                switch (motionEvent.getActionMasked())
                {

                    case MotionEvent.ACTION_DOWN:
                        ClipData data=ClipData.newPlainText("","");
                        View.DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(null);
                        view.startDrag(data,new MyDragShadowBuilder(),view,0);
                        view.setVisibility(View.VISIBLE);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        break;

                }
                return true;
            }
        });
    }

    private void setDragListeners()
    {

        imageedit.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {
                switch (dragEvent.getAction())
                {
                    case DragEvent.ACTION_DRAG_STARTED:
                        loc_of_text_at_x=dragEvent.getX();
                        loc_of_text_at_y=dragEvent.getY();
                        prevX = dragEvent.getX();
                        prevY = dragEvent.getY();
                        break;
                    case DragEvent.ACTION_DRAG_EXITED:
                        if (selected_text)
                        {
                            dragText.setX(prevX);
                            dragText.setY(prevY);
                            dragText.setVisibility(View.VISIBLE);
                        }
                        else if (selected_stickers)
                        {
                            add_stickers.setX(prevX);
                            add_stickers.setY(prevY);
                            add_stickers.setVisibility(View.VISIBLE);
                        }
                        break;
                    case DragEvent.ACTION_DROP:
                        if (selected_text)
                        {
                            dif1_text=dragEvent.getX()-loc_of_text_at_x;
                            diff2_text=dragEvent.getY()-loc_of_text_at_y;
                            dragText.setX(dragText.getX()+dif1_text);
                            dragText.setY(dragText.getY()+diff2_text);
                            dragText.setVisibility(View.VISIBLE);
                        }

                        else if (selected_stickers){
                            diff1_stickers=dragEvent.getX()-loc_of_text_at_x;
                            diff2_stickers=dragEvent.getY()-loc_of_text_at_y;
                            add_stickers.setX(add_stickers.getX()+diff1_stickers);
                            add_stickers.setY(add_stickers.getY()+diff2_stickers);
                            add_stickers.setVisibility(View.VISIBLE);
                            dragText.setVisibility(View.GONE);
                        }
                        break;
                    case DragEvent.ACTION_DRAG_LOCATION:
                        if (selected_text)
                        {
                            dif1_text=dragEvent.getX()-loc_of_text_at_x;
                            diff2_text=dragEvent.getY()-loc_of_text_at_y;
                            loc_of_text_at_x = dragEvent.getX();
                            loc_of_text_at_y = dragEvent.getY();
                            dragText.setX(dragText.getX()+dif1_text);
                            dragText.setY(dragText.getY()+diff2_text);
                            dragText.setVisibility(View.VISIBLE);
                        }
                        else if (selected_stickers){
                            diff1_stickers=dragEvent.getX()-loc_of_text_at_x;
                            diff2_stickers=dragEvent.getY()-loc_of_text_at_y;
                            loc_of_text_at_x = dragEvent.getX();
                            loc_of_text_at_y = dragEvent.getY();
                            add_stickers.setX(add_stickers.getX()+diff1_stickers);
                            add_stickers.setY(add_stickers.getY()+diff2_stickers);
                            add_stickers.setVisibility(View.VISIBLE);
                        }
                        break;
                }
                return true;
            }
        });
    }

    private void setListeners()
    {
        zoom_stickers.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

//                seekBar.setMax(500);
                add_stickers.getLayoutParams().width = progress*2;
                add_stickers.getLayoutParams().height = progress*2;
                add_stickers.requestLayout();
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        zooming_text.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                dragText.setTextSize(TypedValue.COMPLEX_UNIT_SP,i+14);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                builder.setTitle("Confirm Exit");
                builder.setMessage(Html.fromHtml("<font color='#6101e9'>Do you want to save?</font>"));

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        saveFinalImage();
                        if (interstitialAd.isLoaded()) {
                            interstitialAd.show();
                            finish();
                        } else {
                            interstitialAd.show();
                            finish();
                        }
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        FileOutputStream outputStream=null;
                        File sdcard=Environment.getExternalStorageDirectory();
                        File directory=new File(sdcard.getAbsolutePath()+"/MEME/Images");
                        File outputfile=new File(directory,temp_storage);
                        if (outputfile.exists())
                        {
                            outputfile.delete();
                        }
                        if (interstitialAd.isLoaded()) {
                            interstitialAd.show();
                            finish();
                        } else {
                            interstitialAd.show();
                            finish();
                        }
                    }
                });

                builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();
            }


        });

        text_feature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text_editing_relative_layout.setVisibility(View.VISIBLE);
                feature_layout.setVisibility(View.GONE);
                stickers_relative.setVisibility(View.INVISIBLE);
                dragText.setVisibility(View.VISIBLE);
                selected_text=true;
                selected_stickers=false;
            }
        });

        sticker_feature.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                selected_text=false;
                selected_stickers=true;
                searchStickersDialog.showSearchSrickerDialog();
            }
        });


        crop_feature.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                startCropImageActivity();
            }
        });

        close_edit_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text_editing_relative_layout.setVisibility(View.GONE);
                feature_layout.setVisibility(View.VISIBLE);
                add_Text.setText("");
            }
        });

        save_added_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                feature_layout.setVisibility(View.INVISIBLE);
                SavingWatermarkText();
                dragText.setVisibility(View.VISIBLE);
                add_Text.setText(" ");

            }
        });

        add_Text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                String edit = add_Text.getText().toString();
                dragText.setText(edit);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                String edit = add_Text.getText().toString();
                dragText.setText(edit);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String edit = add_Text.getText().toString();
                dragText.setText(edit);
            }
        });

        hide_stickers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stickers_relative.setVisibility(View.INVISIBLE);
                add_stickers.setVisibility(View.INVISIBLE);
                feature_layout.setVisibility(View.VISIBLE);

            }
        });

        save_stickers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_stickers.setVisibility(View.GONE);
                dragText.setVisibility(View.GONE);
                SavingWatermarkImage();
                stickers_relative.setVisibility(View.GONE);
            }
        });

        radioGroupTextAlignment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_btn_center:
                        editTextCaption.setGravity(Gravity.CENTER);
                        break;
                    case R.id.radio_btn_left:
                        editTextCaption.setGravity(Gravity.LEFT);
                        break;
                }
            }
        });

    }
    @OnClick(R.id.save) void saveBtn()
    {
        saveFinalImage();
        Toast.makeText(this, "Edited image saved successfully", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.add_pic_feature) void  openAddPicDialog()
    {
        selected_text=false;
        selected_stickers=true;
        addPicSearchTemplateDialog.showAddPicDialog();
    }

    @OnClick(R.id.save_caption_layout) void onSaveCaptionClicked() {

        String text = "";
        int x = 25;
        String[] strings = editTextCaption.getText().toString().split(" ");
        for (int i = 0; i < strings.length; i++) {
            Log.e("LogMsg", "String1: " + strings[i]);
            if (strings[i].length() == 25) {
                Log.e("LogMsg", "Hello1");
                if (text.isEmpty()) {
                    text = strings[i];
                } else {
                    text = text + "\n" + strings[i];
                    x = x + 25;
                }
            } else if (strings[i].length() < 25) {
                Log.e("LogMsg", "Hello2");
                if (text.length() + strings[i].length() + 1 > x) {
                    text = text + "\n" + strings[i];
                    x = x + 25;
                } else {
                    text = text + " " + strings[i];
                    if (i==0) {
                        text = text.trim();
                    }
                }
            } else {
                Log.e("LogMsg", "Hello1");
                List<String> stringArrayList = new ArrayList<>();
                int index = 0;
                while (index < strings[i].length()) {
                    stringArrayList.add(strings[i].substring(index, Math.min(index + 25,strings[i].length())));
                    index += 25;
                }
                for (int j = 0; j < stringArrayList.size(); j++) {
                    text = text + "\n" + stringArrayList.get(j);
                    x = x + 25;
                }
            }
            if(strings[i].contains("\n")) {
                x = x + 25;
            }
        }

        String[] finalString = text.split("\n");

        editing_image_layout.getLayoutParams().height = editing_image_layout.getLayoutParams().height + 110 * finalString.length;
        editing_image_layout.requestLayout();
        imgViewCaption.getLayoutParams().height = imgViewCaption.getLayoutParams().height + 110 * finalString.length;
        imgViewCaption.requestLayout();
        imgViewCaption.setVisibility(View.VISIBLE);
        captionEditLayout.setVisibility(View.GONE);

        watermarkTexts = new ArrayList<>();

        Bitmap imageEditBitmap = ((BitmapDrawable)imageedit.getDrawable()).getBitmap();
        imageEditBitmap =Bitmap.createScaledBitmap(imageEditBitmap,imageedit.getWidth(),imageedit.getHeight(),false);
        WatermarkImage captionWatermarkImage = new WatermarkImage(imageEditBitmap)
                .setPositionX(imageedit.getX() / imgViewCaption.getWidth())
                .setPositionY(0.07*finalString.length)
                .setSize((float)imageedit.getWidth()/(float)imgViewCaption.getWidth())
                .setImageAlpha(500);
        for (int j = 0; j < finalString.length; j++) {
            Log.e("LogMsg", "String: " + finalString[j]);
            if (editTextCaption.getGravity() == Gravity.CENTER) {
                WatermarkText watermarkText=new WatermarkText(finalString[j])
                        .setPositionX(txtViewImageCaption.getX() / imgViewCaption.getWidth()-0.1)
                        .setPositionY(waterMarkYcoordinate)
                        .setTextFont(R.font.montserrat)
                        .setTextShadow(0.5f, 3, 3, Color.GRAY)
                        .setTextSize(22)
                        .setTextColor(captionColor)
                        .setTextAlpha(250);
                watermarkTexts.add(watermarkText);
            } else {
                WatermarkText watermarkText=new WatermarkText(finalString[j])
                        .setPositionX(0.02)
                        .setPositionY(waterMarkYcoordinate)
                        .setTextFont(R.font.montserrat)
                        .setTextShadow(0.5f, 3, 3, Color.GRAY)
                        .setTextSize(22)
                        .setTextColor(captionColor)
                        .setTextAlpha(250);
                watermarkTexts.add(watermarkText);
            }
            waterMarkYcoordinate = waterMarkYcoordinate + 0.06;
        }
        Bitmap backgroundBitmap = ((BitmapDrawable)imgViewCaption.getDrawable()).getBitmap();
        backgroundBitmap = Bitmap.createScaledBitmap(backgroundBitmap,imgViewCaption.getWidth(),imgViewCaption.getLayoutParams().height,false);
        WatermarkBuilder
                .create(this, backgroundBitmap)
                .loadWatermarkImage(captionWatermarkImage) // use .loadWatermarkImage(watermarkImage) to load an image.
                .loadWatermarkTexts(watermarkTexts)
                .getWatermark()
                .setToImageView(imageedit);
        Bitmap bitmap = WatermarkBuilder
                .create(this, imageedit)
                .getWatermark()
                .getOutputImage();

        saveBitmap(bitmap);
        captionEditLayout.setVisibility(View.GONE);
        doneTxtCaption.setVisibility(View.VISIBLE);
        saveCaption.setVisibility(View.GONE);
        editing_image_layout.getLayoutParams().height = mainLayoutHeigth;
        imgViewCaption.getLayoutParams().height = mainLayoutHeigth;
        editing_image_layout.requestLayout();
        imgViewCaption.requestLayout();
        editTextCaption.setText("");
    }

    private void SavingWatermarkImage()
    {
        Bitmap bitmap=((BitmapDrawable)add_stickers.getDrawable()).getBitmap();
        bitmap =Bitmap.createScaledBitmap(bitmap,add_stickers.getLayoutParams().width, add_stickers.getLayoutParams().height,false);
        WatermarkImage watermarkImage=new WatermarkImage(bitmap)
                .setPositionX(add_stickers.getX() / editing_image_layout.getWidth())
                .setPositionY(add_stickers.getY() / editing_image_layout.getHeight())
                .setSize((float)add_stickers.getLayoutParams().width/(float)editing_image_layout.getWidth())
                .setImageAlpha(500);

        WatermarkBuilder
                .create(getApplicationContext(), imageedit)
                .loadWatermarkImage(watermarkImage)
                .getWatermark()
                .setToImageView(imageedit);

        Bitmap output=WatermarkBuilder
                .create(getApplicationContext(),imageedit)
                .getWatermark()
                .getOutputImage();

        saveBitmap(output);
    }


    private void SavingWatermarkText()
    {
        WatermarkText watermarkText = new WatermarkText(dragText)
                .setPositionY(dragText.getY()/editing_image_layout.getHeight()-0.05)
                .setPositionX(dragText.getX()/editing_image_layout.getWidth())
                .setTextAlpha(250)
                .setTextFont(R.font.montserrat)
                .setTextSize(dragText.getTextSize()-textExtraSize)
                .setTextShadow(0.5f, 3, 3, Color.GRAY)
                .setTextColor(color);

        WatermarkBuilder
                .create(getApplicationContext(), imageedit)
                .loadWatermarkText(watermarkText)
                .getWatermark()
                .setToImageView(imageedit);

        Bitmap output=WatermarkBuilder.create(getApplicationContext(),imageedit)
                .getWatermark().getOutputImage();

        saveBitmap(output);
    }

    @Override
    public void onBackPressed() {
        builder.setTitle("Confirm Exit");
        builder.setMessage(Html.fromHtml("<font color='#6101e9'>Do you want to save?</font>"));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                saveFinalImage();
                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                } else {
                    interstitialAd.show();

                }
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                FileOutputStream outputStream=null;
                File sdcard=Environment.getExternalStorageDirectory();
                File directory=new File(sdcard.getAbsolutePath()+"/MEME/Images");
                File outputfile=new File(directory,temp_storage);
                if (outputfile.exists())
                {
                    outputfile.delete();
                }
                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                } else {
                    interstitialAd.show();
                }
                finish();
            }
        });

        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }



    @Override
    public void onViewClick_textColor(int id)
    {
        for ( int i = 0; i< mTextColour.size(); i++)
        {
            if (mTextColour.get(i).text_id == id)
            {
                mTextColour.get(i).isSelected = true;
                dragText.setTextColor(Color.parseColor(mTextColour.get(i).hex_code));
                color = Color.parseColor(mTextColour.get(i).hex_code);

            } else
            {
                mTextColour.get(i).isSelected = false;
            }
        }
        myAdapter.updateDataSet(mTextColour);
    }

    // for cropping

    private void startCropImageActivity()
    {
        if (isFromGallery) {

            CropImage.activity(imageuri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setMultiTouchEnabled(true)
                    .start(this);
        } else {
            BitmapDrawable drawable = (BitmapDrawable) imageedit.getDrawable();
            Bitmap bitmap = drawable.getBitmap();
            saveBitmap(bitmap);
            CropImage.activity(Uri.fromFile(new File(imageuri.getPath())))
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setMultiTouchEnabled(true)
                    .start(this);
            isFromGallery = true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {

        if (requestCode==REQUESTCODE_Templates && resultCode==RESULT_OK)
        {
            imageuri=data.getData();
            GlideHelper.setImageViewWithURI(this,add_stickers,imageuri);
            add_stickers.setVisibility(View.VISIBLE);
            stickers_relative.setVisibility(View.VISIBLE);
        }
        else if (requestCode==REQUESTCODE_STICKERS && resultCode==RESULT_OK)
        {
            imageuri=data.getData();
            add_stickers.setImageURI(imageuri);
            stickers_relative.setVisibility(View.VISIBLE);
        }
        super.onActivityResult(requestCode, resultCode, data);

        // handle result of CropImageActivity

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE )
        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK)
            {
                ((ImageView) findViewById(R.id.image_editor_layout)).setImageURI(result.getUri());
                drawable=(BitmapDrawable)imageedit.getDrawable();
                bitmap=drawable.getBitmap();
                saveBitmap(bitmap);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }

    }

    @OnClick(R.id.layout_caption_option) void onImageCaptionLayoutClicked() {
        captionEditLayout.setVisibility(View.VISIBLE);

    }

    @OnClick(R.id.done) void captionEditingDone()
    {

        saveCaption.setVisibility(View.VISIBLE);
        doneTxtCaption.setVisibility(View.GONE);
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @OnClick(R.id.hide_caption_layout) void onCancelCaptionLayout() {
        captionEditLayout.setVisibility(View.GONE);
        editTextCaption.setText("");
    }

    @Override
    public void onCaptionColorClicked(int id) {
        for ( int i = 0; i< captionColorArray.size(); i++)
        {
            if (captionColorArray.get(i).text_id == id)
            {
                captionColorArray.get(i).isSelected = true;
                editTextCaption.setTextColor(Color.parseColor(captionColorArray.get(i).hex_code));
                captionColor = Color.parseColor(captionColorArray.get(i).hex_code);

            } else {
                captionColorArray.get(i).isSelected = false;
            }
        }
        imageCaptionColorAdapter.updateDataSet(captionColorArray);
    }

    public void saveBitmap(Bitmap bitmap) {
        FileOutputStream outputStream=null;
        File sdcard=Environment.getExternalStorageDirectory();
        File directory=new File(sdcard.getAbsolutePath()+"/Meme/Images");
        directory.mkdirs();
        File outputfile=new File(directory,temp_storage);
        try {
            outputStream=new FileOutputStream(outputfile);
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
            outputStream.flush();
            outputStream.close();
            imageuri = Uri.fromFile(outputfile);
            Log.e("LogMsg","saving : ");
            Intent intent=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            intent.setData(Uri.fromFile(outputfile));
            this.sendBroadcast(intent);
            Log.e("LogMsg","saved : ");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFileSavingFinish(String uri)
    {
        Intent intent=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(uri);
        intent.setData(Uri.fromFile(file));
        sendBroadcast(intent);
        progresstemplatesDownload.dismiss();
        Intent gridImg=new Intent(ImageEditActivity.this, ImageEditActivity.class);
        gridImg.putExtra("uri",uri);
        gridImg.putExtra("isFromGallery",true);
        startActivity(gridImg);
    }



    @Override
    public void setPicToView(String url) {
        GlideHelper.setImageView(this,add_stickers,url);
        add_stickers.setVisibility(View.VISIBLE);
        addPicSearchTemplateDialog.dismisDialog();
        stickers_relative.setVisibility(View.VISIBLE);
    }

    @Override
    public void chooseTemplate() {
        Intent chooseTemplates=new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        chooseTemplates.setType("image/*");
        startActivityForResult(chooseTemplates,REQUESTCODE_Templates);
        add_stickers.setVisibility(View.VISIBLE);
        addPicSearchTemplateDialog.dismisDialog();
    }


    @Override
    public void setStickerToView(String url) {
        GlideHelper.setImageView(this,add_stickers,url);
        add_stickers.setVisibility(View.VISIBLE);
        searchStickersDialog.dismisDialog();
        stickers_relative.setVisibility(View.VISIBLE);
    }

    @Override
    public void chooseSticker() {
        Intent chooseStickers=new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        chooseStickers.setType("image/*");
        startActivityForResult(chooseStickers,REQUESTCODE_STICKERS);
        add_stickers.setVisibility(View.VISIBLE);
        searchStickersDialog.dismisDialog();

    }


    @Override
    public void loadTemplatesAdapter(TemplatesAdapter adapter, Images[] images) {
        addPicSearchTemplateDialog.setImages(images);
    }

    @Override
    public void loadStickersAdapters(StickersAdapter stickersAdapter, Images[] images) {
        searchStickersDialog.setImages(images);
    }

    private void saveFinalImage() {
        BitmapDrawable drawable = (BitmapDrawable) imageedit.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        saveBitmap(bitmap);
    }

    @Override
    public void onTemplateClicked(String imageUrl) {

    }

    public void setTextExtraSize() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int widthPixels =  displayMetrics.widthPixels;
        if (widthPixels >= 1080) {
            textExtraSize = 40;
        } else {
            textExtraSize = 12;
        }
    }

}
