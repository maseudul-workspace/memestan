package com.webinfotech.memestan.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.webinfotech.memestan.R;
import com.webinfotech.memestan.domain.models.Template_class;
import com.webinfotech.memestan.util.GlideHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ViewHolder>
{
    public interface Callback
    {
        void onViewClick(String imageUrl);
    }
    Context mContext;
    List<Template_class> mTemplates;
    Callback mCallback;

    public ImageListAdapter(Context mContext, List<Template_class> mTemplates, Callback mCallback)
    {
        this.mContext = mContext;
        this.mTemplates = mTemplates;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view;
        LayoutInflater mInflater=LayoutInflater.from(parent.getContext());
        view=mInflater.inflate(R.layout.cardview_item_templates,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        GlideHelper.setImageView(mContext,holder.grid_imageView, mContext.getResources().getString(R.string.base_url) + "template/" + mTemplates.get(position).Image_url);
        holder.grid_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onViewClick(mContext.getResources().getString(R.string.base_url) + "template/" + mTemplates.get(position).Image_url);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mTemplates.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        @BindView (R.id.cardview_image)
        ImageView grid_imageView;
        @BindView(R.id.card_grid_image)
        MaterialCardView grid_card;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }


}
