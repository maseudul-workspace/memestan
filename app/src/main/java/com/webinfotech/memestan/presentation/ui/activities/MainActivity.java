package com.webinfotech.memestan.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.viewpager.widget.ViewPager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.animation.DecelerateInterpolator;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;
import com.webinfotech.memestan.R;
import com.webinfotech.memestan.domain.models.Images;
import com.webinfotech.memestan.presentation.ui.adapters.ViewPagerAdapter;
import com.webinfotech.memestan.presentation.ui.dialog.SearchTemplateDialog;
import com.webinfotech.memestan.presentation.ui.fragments.Images_fragment;
import com.webinfotech.memestan.presentation.ui.fragments.Templates_fragment;
import com.webinfotech.memestan.presentation.ui.fragments.Videos_fragment;

import java.io.File;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener , NestedScrollView.OnScrollChangeListener, Templates_fragment.Callback , SearchTemplateDialog.Callback, Images_fragment.Callback, Videos_fragment.Callback {

    @BindView (R.id.main_nav)
    BottomNavigationView bottomNavigationView;
    Intent pickimage=new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    Uri selectedUri;
    ProgressDialog progresstemplatesDownload;
    int REQUESTCODE_IMAGEHOME=100;
    int REQUESTCODE_VIDEOHOME=101;
    @BindView(R.id.tablayout_id)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    SearchTemplateDialog searchTemplateDialog;
    @BindView(R.id.mainActivityToolbar)
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setupProgressDialog();
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        viewPagerAdapter=new ViewPagerAdapter(getSupportFragmentManager());
        saveFontFile();
        // adding fragment

        viewPagerAdapter.AddFragment(new Templates_fragment(),"Templates");
        viewPagerAdapter.AddFragment(new Images_fragment(), "My Images ");
        viewPagerAdapter.AddFragment(new Videos_fragment(),"My Videos ");

        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);
        // for search custom dialog
        initialiseDialog();
    }


    public void setupProgressDialog() {
        progresstemplatesDownload=new ProgressDialog(this);
        progresstemplatesDownload.setMessage("Please wait");
        progresstemplatesDownload.setTitle("Downloading");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode==REQUESTCODE_IMAGEHOME && resultCode==RESULT_OK)
        {
            selectedUri=data.getData();
            Intent forimage=new Intent(MainActivity.this, ImageEditActivity.class);
            forimage.putExtra("uri",selectedUri.toString());
            forimage.putExtra("isFromGallery",true);
            startActivity(forimage);

        }
        else if (requestCode==REQUESTCODE_VIDEOHOME && resultCode==RESULT_OK)
        {
            selectedUri=data.getData();
            Intent videoEditIntent=new Intent(MainActivity.this, VideoEditActivity.class);
            videoEditIntent.putExtra("videoUri",selectedUri.toString());
            videoEditIntent.putExtra("isFromGallery",true);
            startActivity(videoEditIntent);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item)
    {
       switch (item.getItemId())
       {
           case R.id.imagesedit:
             pickimage.setType("image/*");
             startActivityForResult(pickimage,100);
               break;

           case R.id.videoedit:
               pickimage.setType("video/*");
               startActivityForResult(pickimage,101);
               break;

       }
        return true;
    }


    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

        if (scrollY>oldScrollY)
        {
            bottomNavigationView.animate().translationY(bottomNavigationView.getHeight()).setInterpolator(
                    new DecelerateInterpolator(2)
            ).start();
        }
        if (scrollY<oldScrollY)
        {
            bottomNavigationView.animate().translationY(0).setInterpolator(
                    new DecelerateInterpolator(2)
            ).start();
        }
        if (scrollY==0)
        {

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    public void saveFontFile() {
        File fontFile =new  File(
                Environment.getExternalStorageDirectory(),
                File.separator + "Meme" + File.separator + ".Font" + File.separator + "Roboto.ttf");
        if (!fontFile.exists())
        {
            File sdcard= Environment.getExternalStorageDirectory();
            File directory=new File(sdcard.getAbsolutePath()+"/Meme/.Font/");
            directory.mkdirs();
            File outputfile=new File(directory,"Roboto.ttf");
            try {
                InputStream is =getContentResolver().openInputStream(Uri.parse("android.resource://" + getPackageName() + "/"+R.font.roboto_black));
                byte[] buffer = new byte[is.available()];
                is.read(buffer);
                is.close();
                FileOutputStream fos=new FileOutputStream(outputfile);
                fos.write(buffer);
                fos.close();

            } catch (Exception e) {
            }
        } else {
        }
    }

    @Override
    public void showBottomNav()
    {
        bottomNavigationView.animate().translationY(bottomNavigationView.getHeight()).setInterpolator(
                new DecelerateInterpolator(2)).start();
    }

    @Override
    public void hideBottomNav()
    {
        bottomNavigationView.animate().translationY(0).setInterpolator(
                new DecelerateInterpolator(2)
        ).start();
    }

    @Override
    public void onTemplateClicked(String image_url) {
        Intent myimage=new Intent(MainActivity.this, ImageEditActivity.class);
        myimage.putExtra("uri",image_url);
        myimage.putExtra("isFromGallery",false);
        startActivity(myimage);
    }

    @Override
    public void showDialogClicked() {
        searchTemplateDialog.showDialog();
    }

    @Override
    public void setDialogTemplates(Images[] images) {
        searchTemplateDialog.setIemplates(images);
    }

    public void initialiseDialog()
    {
        searchTemplateDialog=new SearchTemplateDialog(this,this,this);
        searchTemplateDialog.setUpDialog();

    }

    @Override
    public void myImagesCallback(String imageUrl)
    {
       Intent myimage=new Intent(MainActivity.this, ImageEditActivity.class);
       myimage.putExtra("uri",imageUrl);
       myimage.putExtra("isFromGallery",false);
       startActivity(myimage);
    }

    @Override
    public void showBottomNavImages() {
        bottomNavigationView.animate().translationY(bottomNavigationView.getHeight()).setInterpolator(
                new DecelerateInterpolator(2)).start();
    }

    @Override
    public void hideBottomNavImages() {
        bottomNavigationView.animate().translationY(0).setInterpolator(
                new DecelerateInterpolator(2)
        ).start();
    }

    @Override
    public void myVideosCallback(String videoUrl) {
        Intent myvideo=new Intent(MainActivity.this, VideoEditActivity.class);
        myvideo.putExtra("videoUri",videoUrl);
        myvideo.putExtra("isFromGallery",false);
        startActivity(myvideo);
    }

    @Override
    public void showBottomNavVideos() {
        bottomNavigationView.animate().translationY(bottomNavigationView.getHeight()).setInterpolator(
                new DecelerateInterpolator(2)).start();
    }

    @Override
    public void hideBottomNavVideos() {
        bottomNavigationView.animate().translationY(0).setInterpolator(
                new DecelerateInterpolator(2)
        ).start();
    }

    @Override
    public void onSearchTemplateClicked(String imageUrl) {
        Intent myimage=new Intent(MainActivity.this, ImageEditActivity.class);
        myimage.putExtra("uri",imageUrl);
        myimage.putExtra("isFromGallery",false);
        startActivity(myimage);
    }
}
