package com.webinfotech.memestan.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

public class Helper {

    public static String getRealPathFromUri(Context context, Uri contenturi)
    {
        Cursor cursor=null;
        try {

            String [] proj={MediaStore.Images.Media.DATA};
            cursor=context.getContentResolver().query(contenturi,proj,null,null,null);
            int column_index=cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "";
        }
        finally {
            if (cursor !=null)
            {
                cursor.close();
            }
        }
    }

    public static int getVideoWidth(String videoUri) {
        MediaPlayer mp = new MediaPlayer();
        try {
            mp.setDataSource(videoUri);
            mp.prepare();
            return mp.getVideoWidth();
        } catch (Exception e) {
            return 0;
        }
    }

    public static String getRealPath(Context mContext, Uri selectedUri) {
        String uri = "";
        String[] filePathColumn = {MediaStore.Video.Media.DATA};
        Cursor cursor = mContext.getContentResolver().query(selectedUri, filePathColumn, null, null, null);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            uri = cursor.getString(columnIndex);
        }
        return uri;
    }

    public static Uri getVideoContentUri(Context context, String absPath) {

        Cursor cursor = context.getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                , new String[]{MediaStore.Video.Media._ID}
                , MediaStore.Video.Media.DATA + "=? "
                , new String[]{absPath}, null);

        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            return Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, Integer.toString(id));

        } else if (!absPath.isEmpty()) {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Video.Media.DATA, absPath);
            return context.getContentResolver().insert(
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
        } else {
            return null;
        }
    }

}
