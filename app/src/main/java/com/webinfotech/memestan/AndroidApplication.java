package com.webinfotech.memestan;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.webinfotech.memestan.domain.models.Images;

public class AndroidApplication extends Application {

    Images[] templates;
    Images[] stickers;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setTemplates(Context context, Images[] images) {
        this.templates = images;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(images != null) {
            editor.putString(getString(R.string.KEY_TEMPLATES_PREFERENCES), new Gson().toJson(images));
        } else {
            editor.putString(getString(R.string.KEY_TEMPLATES_PREFERENCES), "");
        }

        editor.commit();
    }

    public Images[] getTemplates(Context context){
        Images[] images;
        if(templates != null){
            images = templates;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_APP_PREFERENCES), Context.MODE_PRIVATE);
            String templatesJson = sharedPref.getString(getString(R.string.KEY_TEMPLATES_PREFERENCES),"");
            if(templatesJson.isEmpty()){
                images = null;
            }else{
                try {
                    images = new Gson().fromJson(templatesJson, Images[].class);
                    this.templates = images;
                }catch (Exception e){
                    images = null;
                }
            }
        }
        return images;
    }

    public void setStickers(Context context, Images[] images) {
        this.stickers = images;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(images != null) {
            editor.putString(getString(R.string.KEY_STICKERS_PREFERENCES), new Gson().toJson(images));
        } else {
            editor.putString(getString(R.string.KEY_STICKERS_PREFERENCES), "");
        }

        editor.commit();
    }

    public Images[] getStickers(Context context){
        Images[] images;
        if(stickers != null){
            images = stickers;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_APP_PREFERENCES), Context.MODE_PRIVATE);
            String stickersJson = sharedPref.getString(getString(R.string.KEY_STICKERS_PREFERENCES),"");
            if(stickersJson.isEmpty()){
                images = null;
            }else{
                try {
                    images = new Gson().fromJson(stickersJson, Images[].class);
                    this.stickers = images;
                }catch (Exception e){
                    images = null;
                }
            }
        }
        return images;
    }

}
