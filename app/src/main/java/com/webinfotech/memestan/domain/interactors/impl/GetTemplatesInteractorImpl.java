package com.webinfotech.memestan.domain.interactors.impl;

import com.webinfotech.memestan.domain.executors.Executor;
import com.webinfotech.memestan.domain.executors.MainThread;
import com.webinfotech.memestan.domain.interactors.GetTemplatesInteractor;
import com.webinfotech.memestan.domain.interactors.base.AbstractInteractor;
import com.webinfotech.memestan.domain.models.Images;
import com.webinfotech.memestan.domain.models.ImagesWrapper;
import com.webinfotech.memestan.repository.AppRepositoryImpl;

public class GetTemplatesInteractorImpl extends AbstractInteractor implements GetTemplatesInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;

    public GetTemplatesInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchingTemplatesFail(errorMsg);
            }
        });
    }

    private void postMessage(Images[] templates){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchingTemplatesSuccess(templates);
            }
        });
    }

    @Override
    public void run() {
        final ImagesWrapper imagesWrapper = mRepository.getTemplates();
        if (imagesWrapper == null) {
            notifyError("Something went wrong");
        } else if (!imagesWrapper.status) {
            notifyError(imagesWrapper.message);
        } else {
            postMessage(imagesWrapper.templates);
        }
    }
}
