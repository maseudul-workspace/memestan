package com.webinfotech.memestan.domain.interactors;

import com.webinfotech.memestan.domain.models.Images;

public interface GetStickersInteractor {
    interface Callback {
        void onGettingStickersSuccess(Images[] images);
        void onGettingStickersFail(String errorMsg);
    }
}
