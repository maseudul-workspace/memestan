package com.webinfotech.memestan.domain.models;

public class Text_color_class
{
    public int text_id;
    public String hex_code;
    public boolean isSelected;

    public Text_color_class(int text_id, String hex_code, boolean isSelted) {

        this.text_id = text_id;
        this.hex_code = hex_code;
        this.isSelected = isSelted;
    }
}
