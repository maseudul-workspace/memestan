package com.webinfotech.memestan.domain.interactors.impl;

import com.webinfotech.memestan.domain.executors.Executor;
import com.webinfotech.memestan.domain.executors.MainThread;
import com.webinfotech.memestan.domain.interactors.GetStickersInteractor;
import com.webinfotech.memestan.domain.interactors.base.AbstractInteractor;
import com.webinfotech.memestan.domain.models.Images;
import com.webinfotech.memestan.domain.models.ImagesWrapper;
import com.webinfotech.memestan.repository.AppRepositoryImpl;

public class GetStickersInteractorImpl extends AbstractInteractor implements GetStickersInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;

    public GetStickersInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingStickersFail(errorMsg);
            }
        });
    }

    private void postMessage(Images[] templates){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingStickersSuccess(templates);
            }
        });
    }

    @Override
    public void run() {
        final ImagesWrapper imagesWrapper = mRepository.getStickers();
        if (imagesWrapper == null) {
            notifyError("Something went wrong");
        } else if (!imagesWrapper.status) {
            notifyError(imagesWrapper.message);
        } else {
            postMessage(imagesWrapper.templates);
        }
    }
}
