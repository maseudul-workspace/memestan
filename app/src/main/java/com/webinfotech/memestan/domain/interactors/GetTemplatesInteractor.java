package com.webinfotech.memestan.domain.interactors;

import com.webinfotech.memestan.domain.models.Images;

public interface GetTemplatesInteractor {
    interface Callback {
        void onFetchingTemplatesSuccess(Images[] templates);
        void onFetchingTemplatesFail(String errorMsg);
    }
}
